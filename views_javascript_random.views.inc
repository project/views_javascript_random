<?php

/**
 * Implementation of hook_views_data
 */
function views_javascript_random_views_data() {
  $data['views_javascript_random']['table']['group'] = t('Global');
  $data['views_javascript_random']['table']['join'] = array(
    '#global' => array(),
  );

  $data['views_javascript_random']['js_random'] = array(
    'title' => t('Javascript Random'),
    'help' => t('Use Javascript to randomize the display order.'),
    'sort' => array(
      'handler' => 'views_javascript_random_views_handler_sort_js_random',
    ),
  );

  return $data;
}

/**
 * Implementation of hook_views_handlers
 */
function views_javascript_random_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'views_javascript_random') .'/handlers',
    ),
    'handlers' => array(
      'views_javascript_random_views_handler_sort_js_random' => array(
        'parent' => 'views_handler_sort',
      ),
    ),
  );
}

