
function vjr_oc(a) {
  var o = {};
  for(var i=0; i < a.length; i++) {
    o[a[i]] = '';
  }
  return o;
}

function vjr_get_random(max, rows) {
  if (max < rows) {
    rows = max;
  }
  var numbers = new Array();
  while (numbers.length < rows) {
    var rand = Math.floor(Math.random() * max);
    while (rand in vjr_oc(numbers)) {
      var rand = Math.floor(Math.random() * max);
    }
    numbers.push(rand);
  }
  return numbers;
}

function vjr_go() {
  var views_javascript_random = Drupal.settings.views_javascript_random;

  for (var i = 0; i < views_javascript_random.length; i++) {
    // Do not process unless all 3 varables are defined in the arrays.
    if (typeof views_javascript_random[i][0] == 'undefined') {
      // No view class selector.
      continue;
    }
    if (typeof views_javascript_random[i][1] == 'undefined') {
      // Number to display not set.
      continue;
    }
    if (typeof views_javascript_random[i][2][0] == 'undefined') {
      // Array of rows in the view not set.
      continue;
    }

    // Get random numbers
    var max = views_javascript_random[i][2].length;
    var rows = views_javascript_random[i][1];
    var selector = views_javascript_random[i][0]
    var rand_numbers = vjr_get_random(max, rows);
//     alert(views_javascript_random[i][0] + ' ' + max + ' ' + rows + ' ' + rand_numbers);

    // Check to see if the view has already been processed.
    var processed = 0;
    $('.' + selector + ' .js_rand_row').each(function() {
      if (this.style.display == 'block') {
        processed = 1;
      }
    });

    // Turn on selected rows
    if (processed == 0) {
      var counter = 0;
      $('.' + selector + ' .js_rand_row').each(function() {
        if (counter in vjr_oc(rand_numbers)) {
          this.style.display = 'block';
        }
        counter++;
      });

      // Remove this from the array as it has been processed.
      views_javascript_random.splice(i, 1);
    }
  }
}